# Stopky pomocí 7seg

## 📖 Základní info o projektu
Jako projekt jsem vytvořil stopky počítající do 59:59 minut. Stopky lze tlačítkem pozastavit a opětovným zmáčknutím resetovat. Na výstupu jsou 4 Sedmisegmentové displeje ovládané pomocí dekodérů.
Projekt jsem realizoval na navrhnuté mnou vytvořené DPS a také i na nepájivém poli.

```mermaid
flowchart LR
    Tlačítko-->E4
    E4 --> STM8
subgraph Nucleo
    STM8-->G0-3
    STM8-->E0-3
    STM8-->B0-3
    STM8-->F3-6
    end

subgraph Dekodery
    G0-3-->1.Dekoder
    E0-3-->2.Dekoder
    B0-3-->3.Dekoder
    F3-6-->4.Dekoder
    end
subgraph Sedmisegmenty
    1.Dekoder-->1.Sedmisegment
    2.Dekoder-->2.Sedmisegment
    3.Dekoder-->3.Sedmisegment
    4.Dekoder-->4.Sedmisegment
    end

```

## 📄 Tabulka použitých součástek
1) DPS  

|   Typ součástky  |      Počet kusů      |  Cena/1 ks  | Cena  |       
|:---------------: |:--------------------:|:-----------:|:-----:|       
|    Rezistor      |    28                | 2    Kč     | 56 Kč |       
|   Dekodér        |    4                 |  19 Kč      | 76 Kč |       
|Patice na dekodér |    4                 | 2 Kč        | 8 Kč  |       
|Segmentový displej|    4                 | 15 Kč       | 60 Kč |          
|Tlačítko          |    1                 |  3 Kč       | 3 Kč  |               
|STM8 Nucleo       |    1                 |  250 Kč     | 250 Kč|  

2) Nepájivé pole    
                                                              
|   Typ součástky  |      Počet kusů      |  Cena/1 ks  | Cena  |       
|:---------------: |:--------------------:|:-----------:|:-----:|       
|    Rezistor      |    28                | 2    Kč     | 56 Kč |       
|   Dekodér        |    4                 |  19 Kč      | 76 Kč |       
|Nepájivé pole     |    1                 | 350 Kč      | 350 Kč  |       
|Segmentový displej|    4                 | 15 Kč       | 60 Kč |          
|Tlačítko          |    1                 |  3 Kč       | 3 Kč  |               
|STM8 Nucleo       |    1                 |  250 Kč     | 250 Kč| 


## ✏  Schéma v KiCadu
![](img/kicad.png)

## 🎨 Návrh DPS
![](img/navrh.png)

## 📷 Fotky realizace
### 1) Funkční zapojení na nepájivém poli
    (připojení pouze 2 sedmisegmentových displejů, pro větší přehlednost)

![](img/zapojeni.jpg)
### 2) Osazená DPS 🤒

![](img/DPS.jpg)

## Zdrojový kód
    Zdrojový kód je součástí přílohy.

## Závěr
Tento projekt byl pro mě opravdovou výzvou.
Při práci jsem narazil na některé mé nedostatky, například osazování DPS :)). S Cčkem si navzájem moc nerozumíme... Ze začátku samotný program vypadal úplně jinak, dělal si co chtěl a házel časté chyby. Avšak po několika hodinových konzultacíh se spolužáky program začal fungoval podle našich představ. Celkově si myslím, že jsem projekt zvládl. Práci na něm odhaduji na několik desítek hodin, i přes to mě práce na něm celkem bavila :)).


## Autor
  Matěj Zgabaj
[**tajne**](https://www.youtube.com/watch?v=dQw4w9WgXcQ)



