#include "stm8s.h"
#include "custom.h"

void delay_s_init(void){ 
    TIM3_TimeBaseInit(TIM3_PRESCALER_256,62499);
    TIM3_Cmd(ENABLE);
    
}
void delay_s(uint32_t time_s){
    TIM3_SetCounter(0);
    for(uint32_t i=0;i<time_s;i++){
        while (TIM3_GetFlagStatus(TIM3_FLAG_UPDATE) != SET)
                {;}
        TIM3_ClearFlag (TIM3_FLAG_UPDATE);
    }
}